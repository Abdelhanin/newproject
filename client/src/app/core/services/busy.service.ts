import { Injectable } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
@Injectable({
  providedIn: 'root'
})
export class BusyService {
  busyRequestAccount = 0;

  constructor(private spinnerService: NgxSpinnerService) { }

  busy(): void {
    this.busyRequestAccount++;
    this.spinnerService.show(undefined, {
      type: 'pacman',
      bdColor: 'rgba(255,255,255,0.7)',
      color: '#333333'
    });
  }

  idle(): void {
    this.busyRequestAccount--;
    if (this.busyRequestAccount <= 0) {
      this.busyRequestAccount = 0;
      this.spinnerService.hide();
    }
  }
}
