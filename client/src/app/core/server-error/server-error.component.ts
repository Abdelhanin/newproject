import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-server-error',
  templateUrl: './server-error.component.html',
  styleUrls: ['./server-error.component.scss']
})
export class ServerErrorComponent implements OnInit {
  error: any;

  constructor(private router: Router) {
    // extras router is valid only in construtor
    const navigation = this.router.getCurrentNavigation();
    this.error = navigation?.extras?.state?.error; // equi valent de navigation && navigation.extras && navigation.extras.error
  }

  ngOnInit(): void {
  }

}
