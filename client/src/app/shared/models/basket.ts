import { v4 as uuidv4 } from 'uuid';
import { IBasketItem } from './basket-item';


export interface IBasket {
  id: string;
  items: IBasketItem[];
}


export class Basket implements IBasket {
  id = uuidv4();
  items: IBasketItem[] = [];
}


export interface IBasketTotals {
  shipping: number;
  subtotal: number;
  total: number;
}
